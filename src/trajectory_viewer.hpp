#pragma once

#include "srrg_system_utils/system_utils.h"
#include "srrg_core_viewers/cloud_viewer.h"
#include "srrg_gl_helpers/opengl_primitives.h"


//ds satisfy Eigen since our viewers are aligned
typedef std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > IsometryVector;

//ds our own little viewer class
//ds defined here for simplicity, in practice put it in a separate .h/.cpp file!
class TrajectoryViewer: public srrg_core_viewers::SimpleViewer {

    //ds object handling
public:

    //ds construct viewer
    TrajectoryViewer() {
       setWindowTitle("Trajectory Viewer");
        _poses_ground_truth.clear();
        _poses_estimated.clear();

    }
    ~TrajectoryViewer() {
        //ds no dynamically allocated memory to clean
    }

    //ds access
public:

    //ds update displayed data - in the worst case, like here, copying it if ownership is not defined
    void updatePoses(IsometryVector& new_poses_) {
        _poses_ground_truth.insert(_poses_ground_truth.end(), new_poses_.begin(), new_poses_.end());
        new_poses_.clear();
    }

    //ds update displayed data - in the worst case, like here, copying it if ownership is not defined
    void updatePoses_est(IsometryVector& new_poses_est) {
        _poses_estimated.insert(_poses_estimated.end(), new_poses_est.begin(), new_poses_est.end());
        new_poses_est.clear();
    }

    //ds overwrites
protected:

    //ds Qt standard draw function
    virtual void draw() override {
        //glColor3f(0, 0, 0);
        glPointSize(0.1);
        glLineWidth(0.1);
        drawAxis(0.1);

        if (!_poses_ground_truth.empty()) {

            //ds draw ground truth in red
            glColor3f(1,0, 0);
            for (const Eigen::Isometry3d& pose: _poses_ground_truth) {
              glPushMatrix();
              glMultMatrixf(pose.cast<float>().data());
              srrg_gl_helpers::drawPyramidWireframe(0.01, 0.01);
              glPopMatrix();
            }
            Eigen::Isometry3d previous_pose(_poses_ground_truth.front());
            glBegin(GL_LINES);
            glColor3f(1, 0, 0);
            for (const Eigen::Isometry3d& pose: _poses_ground_truth) {
                glVertex3f(previous_pose.translation().x(), previous_pose.translation().y(), previous_pose.translation().z());
                glVertex3f(pose.translation().x(), pose.translation().y(), pose.translation().z());
                previous_pose = pose;

            }
            glEnd();
}
  if (!_poses_estimated.empty()) {


            //ds estimate in blue
            glColor3f(0, 1, 0);

            for (const Eigen::Isometry3d& pose: _poses_estimated) {
              glPushMatrix();
              glMultMatrixf(pose.cast<float>().data());
              srrg_gl_helpers::drawPyramidWireframe(0.01, 0.01);
              glPopMatrix();
            }
             //ds connect poses (highlight trajectory) stimated pose in blue
            Eigen::Isometry3d previous_pose_est(_poses_estimated.front());
            glBegin(GL_LINES);
            glColor3f(0,1,0 );
            for (const Eigen::Isometry3d& pose: _poses_estimated) {
                glVertex3f(previous_pose_est.translation().x(), previous_pose_est.translation().y(), previous_pose_est.translation().z());
                glVertex3f(pose.translation().x(), pose.translation().y(), pose.translation().z());
                previous_pose_est = pose;

            }
            glEnd();
            }

    }

    //ds Qt help string - appears when you press [H] on the viewer
    virtual QString helpString() const override {
        return "A little trajectory example viewer, please describe me better.";
    }

    //ds displayed data
protected:

    //ds a vector of poses
    IsometryVector  _poses_ground_truth;
    IsometryVector  _poses_estimated;
};
