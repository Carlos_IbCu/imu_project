#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Vector3.h"
#include <qapplication.h>
#include <iostream>
#include <cmath>
#include <chrono>
#include "ros/console.h"

#include "imu_node.hpp"
#include "trajectory_viewer.hpp"
#include "imu_filter.hpp"

#include <tf/transform_listener.h>
#include <Eigen/Geometry>
#include <time.h>

#define _USE_MATH_DEFINES
using namespace std_msgs;

//Global objects
imu_node _IMU;                      //This object is used to keep the IMU variables
imu_node nav_update;                //This object is used to keep the correction position
ImuFilter filter_;                  //This object is used to integrate the position
IsometryVector poses_ground_truth;  //This vector is used to keep the real positions in order to plot them in the viewer
IsometryVector _poses_estimated;    //This vector is used to keep the estimated positions in order to plot them in the viewer


//################################ IMU INTEGRATION##########################################################

void CallbackInertial(const sensor_msgs::ImuConstPtr& msg){

	//Extract the data from the topic
	const geometry_msgs::Vector3& ang_vel = msg->angular_velocity;
	const geometry_msgs::Vector3& lin_acc = msg->linear_acceleration;

	//ds Calculate the time between two consecutive messages
	_IMU.dt = msg->header.stamp.toSec() - _IMU.imu_.header.stamp.toSec();
	if(_IMU.imu_.header.stamp.sec == 0){
		_IMU.dt = 0.0;
	}
	_IMU.imu_.header=msg->header;


	//ds Initialize the orientation and the normalization quaternion


	if(_IMU.count==0){

		//Initialize the first of the body reference frame
		//ZYX: 180º 0º 90º
		geometry_msgs::Quaternion init_q;
		init_q.w=0;
		init_q.x=0;
		init_q.y=0.7071068;
		init_q.z=0.7071068;
		filter_.setOrientation(init_q.w, init_q.x, init_q.y, init_q.z);

		_IMU.p0_x=0;
		_IMU.p0_y=0;
		_IMU.p0_z=0;

		_IMU.px=_IMU.p0_x;
		_IMU.py=_IMU.p0_y;
		_IMU.pz=_IMU.p0_z;

		_IMU.vx=0;
		_IMU.vy=0;
		_IMU.vz=0;

		_IMU.x_acc_offset=lin_acc.x;
		_IMU.y_acc_offset=lin_acc.y;
		_IMU.z_acc_offset=lin_acc.z;

		_IMU.x_rot_offset=ang_vel.x;
		_IMU.y_rot_offset=ang_vel.y;
		_IMU.z_rot_offset=ang_vel.z;
	}
	int num_initial_measures=200;

	//Define the offset input sensor in order to calibrate it
	//if(_IMU.count<num_initial_measures){
		/*_IMU.x_acc_offset+=lin_acc.x;
		_IMU.y_acc_offset+=lin_acc.y;
		_IMU.z_acc_offset+=lin_acc.z;

		_IMU.x_rot_offset+=ang_vel.x;
		_IMU.y_rot_offset+=ang_vel.y;
		_IMU.z_rot_offset+=ang_vel.z;

	//}else{
	_IMU.x_acc_offset=_IMU.x_acc_offset/num_initial_measures;
	_IMU.y_acc_offset=_IMU.y_acc_offset/num_initial_measures;
	_IMU.z_acc_offset=_IMU.z_acc_offset/num_initial_measures;

	_IMU.x_rot_offset=_IMU.x_rot_offset/num_initial_measures;
	_IMU.y_rot_offset=_IMU.y_rot_offset/num_initial_measures;
	_IMU.z_rot_offset=_IMU.z_rot_offset/num_initial_measures;
*/
	ROS_INFO("x_bias: [%lf],y_bias: [%lf],z_bias: [%lf]",_IMU.x_acc_offset,_IMU.y_acc_offset,_IMU.z_acc_offset);

	double x_rot=ang_vel.x-_IMU.x_rot_offset;
	double y_rot=ang_vel.y-_IMU.y_rot_offset;
	double z_rot=ang_vel.z-_IMU.z_rot_offset;

	double x_l_acc=lin_acc.x-_IMU.x_acc_offset;
	double y_l_acc=lin_acc.y-_IMU.y_acc_offset;
	double z_l_acc=lin_acc.z-_IMU.z_acc_offset;

	ROS_INFO("raw ax: [%lf], raw ay: [%lf], raw az: [%lf]",lin_acc.x,lin_acc.y,lin_acc.z);
	ROS_INFO("calibrated ax: [%lf], raw ay: [%lf], raw az: [%lf]",x_l_acc,y_l_acc,z_l_acc);

	float dt = _IMU.dt;
	filter_.madgwickAHRSupdateIMU(x_rot,y_rot,z_rot,x_l_acc,y_l_acc,z_l_acc,dt);

	double q0,q1,q2,q3;
	filter_.getOrientation(q0,q1,q2,q3);

	Eigen::Quaterniond EstimatedQ(q0,q1,q2,q3);

	EstimatedQ.normalize();

	Eigen::Quaterniond RotBack = EstimatedQ.conjugate();

	RotBack.normalize();

	Eigen::Vector3d acceleration(x_l_acc,y_l_acc,z_l_acc);

	acceleration.transpose();

	Eigen::Vector3d RotatedAcceleration;

	RotatedAcceleration = RotBack.toRotationMatrix() * acceleration;

	//Eigen::Vector3d RotatedAcc = RotatedAcceleration.vec();

	double aox = RotatedAcceleration[0];
	double aoy = RotatedAcceleration[1];
	double aoz = RotatedAcceleration[2];

	ROS_INFO("aox: [%lf], aoy: [%lf], aoz: [%lf]",aox,aoy,aoz);

	_IMU.integratePosition(aox,aoy,aoz);

	Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

	pose.translation().x()=_IMU.px;
	pose.translation().y()=_IMU.py;
	pose.translation().z()=_IMU.pz;

	ROS_INFO("px: [%lf], py: [%lf], pz: [%lf]",_IMU.px,_IMU.py,_IMU.pz);
	ROS_INFO("....................................");

	pose.rotate(EstimatedQ.toRotationMatrix());

	_poses_estimated.push_back(pose);
	//}

	_IMU.count++;
}




//##########################################  MAIN  ##########################################################
//############################################################################################################

int32_t main(int32_t argc_, char** argv_) {

	//Load the parameters from the yaml file
	//YAML::Node configuration = YAML::LoadFile("/home/carlos/catkin_ws/src/imu_estimator/src/imu_parameter.yaml");
	//const std::string topic_imu =  configuration["main_parameters"]["topic_imu"].as<std::string>();
	std::string topic_imu = "/imu0";
	ros::init(argc_, argv_, "imu_integrator_node");


	ros::NodeHandle node_imu;
	ros::Subscriber subs_imu = node_imu.subscribe(topic_imu,10,CallbackInertial);

	// VIEWER INITIALIZATION
	//	poses_ground_truth.clear();
	_poses_estimated.clear();

	//ds check for invalid call and display help
	if (argc_ > 2) {
		std::cerr << "trajectory_viewer\n"
				"shows a sequence of randomlyrated 3D (camera) poses \n"
				"usage: \n"
				" ./trajectory_viewer <number_of_generated_poses>" << std::endl;
		return 0;
	}

	//ds launch Qt
	QApplication app(argc_, argv_);

	//ds create our viewer
	std::cerr << "Qt initialized, launching viewer" << std::endl;
	TrajectoryViewer viewer;
	viewer.show();

	while (viewer.isVisible() && node_imu.ok()) {

		//ds trigger callbacks
		ros::spinOnce();

		//ds update viewer data
		//		viewer.updatePoses(poses_ground_truth);
		viewer.updatePoses_est(_poses_estimated);

		//ds update visualization
		viewer.updateGL();
		app.processEvents();
	}
	std::cerr << "terminated viewer" << std::endl;
	return 0;

}
