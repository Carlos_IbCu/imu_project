#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Vector3.h"
#include <qapplication.h>
#include <iostream>
#include <cmath>
#include <chrono>
#include "ros/console.h"

#include "imu_node.hpp"
#include "trajectory_viewer.hpp"
#include "imu_filter.hpp"

#include <tf/transform_listener.h>
#include <Eigen/Geometry>
#include <time.h>

#define _USE_MATH_DEFINES
using namespace std_msgs;

imu_node laser_;                    //This object is used to keep the laser variables
imu_node _IMU;                      //This object is used to keep the IMU variables
imu_node V1_;                       //This object is used to keep the vicon variables
imu_node nav_update;                //This object is used to keep the correction position
ImuFilter filter_;                  //This object is used to integrate the position

IsometryVector poses_ground_truth;  //This vector is used to keep the real positions in order to plot them in the viewer
IsometryVector _poses_estimated;    //This vector is used to keep the estimated positions in order to plot them in the viewer


bool is_initialized = false;
bool is_initialized_dataset = false; // Is initialize when the Dataset data is received (For the option 1)
bool is_initialized_imu = false;     // Is initialize when IMU data is received (For the option 2)
bool imu_norm = false;

Eigen::Isometry3d initial_pose;



//############################## VICON (option==2 && option==3) ########################

void callbackTransformStamped(const geometry_msgs::TransformStampedConstPtr& transform_){

	/*Until we received the IMU data anything is plot*/
	if (!is_initialized_imu) {
		return;
	}

	ros::NodeHandle nv1;

	int countV1; // This counter helps us to know in which loop (frame) we are
	nv1.getParam("countV1",countV1);
	int option;
	nv1.getParam("option",option);


	double tlx,tly,tlz;
	double q0,q1,q2,q3;

	tlx=transform_->transform.translation.x;
	tly=transform_->transform.translation.y;
	tlz=transform_->transform.translation.z;

	q0=transform_->transform.rotation.w;
	q1=transform_->transform.rotation.x;
	q2=transform_->transform.rotation.y;
	q3=transform_->transform.rotation.z;

	Eigen::Quaterniond quat(q0,q1,q2,q3);

	quat.normalize();

	Eigen::Matrix3d R= quat.toRotationMatrix();

	double roll,pitch,yaw;

	V1_.quat2euler(quat,roll,pitch,yaw);

	if(countV1==0){
		V1_.p0_x=tlx;
		V1_.p0_y=tly;
		V1_.p0_z=tlz;
	}
	//ds initialize the first position in (0,0,0) for our viewer
	V1_.px= tlx-V1_.p0_x;
	V1_.py= tly-V1_.p0_y;
	V1_.pz= tlz-V1_.p0_z;

	if (option==3){

		//ds save the real position in order to keep updating IMU integration
		nav_update.real_pos.translation.x=V1_.px;
		nav_update.real_pos.translation.y=V1_.py;
		nav_update.real_pos.translation.z=V1_.pz;
		nav_update.real_pos.rotation.w=q0;
		nav_update.real_pos.rotation.x=q1;
		nav_update.real_pos.rotation.y=q2;
		nav_update.real_pos.rotation.z=q3;
	}

	ROS_INFO("V1 px : [%lf], py : [%lf],pz : [%lf]", V1_.px, V1_.py,V1_.pz);

	Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

	pose.translation().x()= V1_.px;
	pose.translation().y()= V1_.py;
	pose.translation().z()= V1_.pz;

	pose.rotate(R);

	poses_ground_truth.push_back(pose);
	if (!is_initialized) {
		initial_pose   = pose;
		is_initialized = true;
	}
	countV1++;
	nv1.setParam("countV1",countV1);
}


//Callback####################LASER LEICA· (option==1)#########################

void callbackPointStamped(const geometry_msgs::PointStampedConstPtr& position_){

	ros::NodeHandle npl;
	int option;
	npl.getParam("option",option);

	if(option==2||option==3){                                   //If the chose option in not the 1 it returns
		return;
	}

	ROS_INFO("Callback laser");

	Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

	/*ds That filter the points that comes from noise and initialize the received position in the origin of the reference frame*/

	if (imu_norm){
		//ds filter the noise position
		int filter_l=0;

		laser_.cp_x=position_->point.x;                         //Set the current x position as the x position received
		laser_.cp_y=position_->point.y;
		laser_.cp_z=position_->point.z;

		if(laser_.loop==0){                                     //Check if it is the first loop and set the initial position
			laser_.p0_x=laser_.cp_x;
			laser_.p0_y=laser_.cp_y;
			laser_.p0_z=laser_.cp_z;
			laser_.pp_x=laser_.cp_x;
			laser_.pp_y=laser_.cp_y;
			laser_.pp_z=laser_.cp_z;
			laser_.loop++;
		}

		double diff_x= fabs(laser_.cp_x-laser_.pp_x);           //Measure the distance between the current position with the previous one
		double diff_y= fabs(laser_.cp_y-laser_.pp_y);
		double diff_z= fabs(laser_.cp_z-laser_.pp_z);

		/*We set our filter resolution in 1.5m it means that if the distance between two consecutive points is bigger
		  than that resolution that point will be remove (because it is not considered  as a real point just noise )
		  and will be taken as the real one the previous one*/

		if((diff_x>1.5)||(diff_y>1.5)||(diff_z>1.5)||(laser_.cp_z<-0.5)){
			filter_l=1;
		}

		if(filter_l==1){
			laser_.cp_x=laser_.pp_x;
			laser_.cp_y=laser_.pp_y;
			laser_.cp_z=laser_.pp_z;
		}else{
			laser_.pp_x=laser_.cp_x;
			laser_.pp_y=laser_.cp_y;
			laser_.pp_z=laser_.cp_z;
		}
		if(option==1){

			//ds save the actual position initializing it in (0,0,0)
			laser_.px = laser_.cp_x-laser_.p0_x;
			laser_.py= laser_.cp_y-laser_.p0_y;
			laser_.pz= laser_.cp_z-laser_.p0_z;

			if(laser_.pz>-0.5){
				pose.translation().x()=laser_.px;
				pose.translation().y()=laser_.py;
				pose.translation().z()=laser_.pz;
			}
			if(is_initialized){
				ROS_INFO("Real px: [%lf], py:[%lf], pz:[%lf]",laser_.px,laser_.py,laser_.pz);
			}
		}

		poses_ground_truth.push_back(pose);

	}
	if (!is_initialized) {
		initial_pose   = pose;
		is_initialized = true;
	}
}


//################################ IMU INTEGRATION##########################################################

void filter_function(const sensor_msgs::ImuConstPtr& msg){

	//ds Extract the option we chose in order to know which calibration parameters execute
	ros::NodeHandle np;
	int option;
	np.getParam("option",option);

	//ds To initialize both callbacks at the same time
	if (option==1){
		if (!is_initialized_dataset) {
			return;
		}
	}
	if ((option==2 && !is_initialized_imu)||(option==3 && !is_initialized_imu)){

		is_initialized_imu = true;

	}

	//Extract the data from the topic
	const geometry_msgs::Vector3& ang_vel = msg->angular_velocity;
	const geometry_msgs::Vector3& lin_acc = msg->linear_acceleration;

	//ds Calculate the time between two consecutive messages
	_IMU.dt = msg->header.stamp.toSec() - _IMU.imu_.header.stamp.toSec();
	if(_IMU.imu_.header.stamp.sec == 0){
		_IMU.dt = 0.0;
	}
	_IMU.imu_.header=msg->header;


	int count;
	np.getParam("count",count);                                               // Get the count to know in which loop we are

	//ds Initialize the orientation and the normalization quaternion
	if(option==1||option==2||option==3){
		ROS_INFO("Wrong if!!!!!!!!!!!!!");
		if (count==0)
		{

			geometry_msgs::Quaternion init_q;
			init_q.w=1;
			init_q.x=0;
			init_q.y=0;
			init_q.z=0;
			filter_.setOrientation(init_q.w, init_q.x, init_q.y, init_q.z);
			_IMU.qNorm.w()=1;
			_IMU.qNorm.x()=0;
			_IMU.qNorm.y()=0;
			_IMU.qNorm.z()=0;
			//ROS_INFO("Enter in the count==0");


			_IMU.f_rotation.w()=1;_IMU.f_rotation.x()=0;_IMU.f_rotation.y()=0;_IMU.f_rotation.z()=0;


		}

		//ds Execute the Madgwick filter to extract the orientation from angular velocity and linear acceleration
		float dt=_IMU.dt;
		filter_.madgwickAHRSupdateIMU(ang_vel.x, ang_vel.y, ang_vel.z,lin_acc.x, lin_acc.y, lin_acc.z,dt);

		/*ds Take out the orientation from our filter class in order to get the projected acceleration from our
	local reference frame to main reference frame*/
		double q0,q1,q2,q3;
		filter_.getOrientation(q0,q1,q3,q3);
		_IMU.c_rotation.w()=q0;
		_IMU.c_rotation.x()=q1;
		_IMU.c_rotation.y()=q2;
		_IMU.c_rotation.z()=q3;

		//Here starts the calibration parameter selection in function of the option you chose


		if(option==1){                                                            // IMU integration vs Laser Leica
			//Initial quaternion for option 1
			_IMU.qNorm.w()=1;
			_IMU.qNorm.x()=0;
			_IMU.qNorm.y()=0;
			_IMU.qNorm.z()=0;
			_IMU.qNorm.normalize();


			//ds to determine the initial position
			_IMU.p0x=0;
			_IMU.p0y=0;
			_IMU.p0z=0;
			imu_norm= true;
		}

		if(option==2){                                                            // IMU integration (without correction) vs Vicon

			//MODIFICAR ESTE QUATERNION
			_IMU.qNorm.w()=0.993240;                                              // To normalize my orientation with the Vicon orientation
			_IMU.qNorm.x()=-0.009296;
			_IMU.qNorm.y()=0.022575;
			_IMU.qNorm.z()=0.113480;

			_IMU.qNorm.normalize();


			//ds to determine the initial position to determine the normaliza position
			_IMU.p0x=0;
			_IMU.p0y=0;
			_IMU.p0z=0;

			double yaw_norm,pitch_norm,roll_norm;
			_IMU.quat2euler(_IMU.qNorm,roll_norm,pitch_norm,yaw_norm);


			_IMU.roll_norm=roll_norm;
			_IMU.pitch_norm=pitch_norm;
			_IMU.yaw_norm=yaw_norm;

			imu_norm= true;
		}

		if(option==3){                                                            // IMU integration (with correction) vs Vicon

			_IMU.qNorm.w()=1;
			_IMU.qNorm.x()=0;
			_IMU.qNorm.y()=0;
			_IMU.qNorm.z()=0;

			_IMU.qNorm.normalize();

			//ds to determine the initial position to determine the normalized position
			_IMU.p0x=0;
			_IMU.p0y=0;
			_IMU.p0z=0;

			double yaw_norm,pitch_norm,roll_norm;
			_IMU.quat2euler(_IMU.qNorm,roll_norm,pitch_norm,yaw_norm);


			_IMU.roll_norm=roll_norm;
			_IMU.pitch_norm=pitch_norm;
			_IMU.yaw_norm=yaw_norm;

			imu_norm= true;

		}

		Eigen::Quaterniond EstimatedQ(q0,q1,q3,q3);

		EstimatedQ.normalize();

		Eigen::Quaterniond Qviz=_IMU.qNorm* EstimatedQ;

		if(option==1){

			//This quaternion is used to compensate the initial quaternion
			Eigen::Quaterniond CompensationQ(0.7539023,0.0,0.0,-0.6569866);

			CompensationQ.normalize();

			Qviz=CompensationQ*Qviz;
		}
		if(option==2||option==3){

			//This quaternion is used to compensate the initial quaternion
			Eigen::Quaterniond CompensationQ(1,0,0,0);

			CompensationQ.normalize();

			Qviz=CompensationQ*Qviz;
		}

		Qviz.normalize();

		//ROTATE THE ACCELERATION VECTOR INTO THE WORLD FRAME-------------------------------------------

		/*As the linear acceleration received from the IMU refers to the body frame we have
		 * to project this acceleration into the inertial frame by rotating the acceleration vector by the
		 * conjugate of our current quaternion orientation
		 */

		//Change 1_  Conjugated quaternion
		//double q0_c=Qviz.w();
		//double q1_c=-1.0*Qviz.x();
		//double q2_c=-1.0*Qviz.y();
		//double q3_c=-1.0*Qviz.z();

		//////////////////////////////////////////////////////////////////////////////////////////////
		_IMU.f_rotation*=_IMU.c_rotation;

		double q0_c=_IMU.f_rotation.w();
		double q1_c=-1.0*_IMU.f_rotation.x();
		double q2_c=-1.0*_IMU.f_rotation.y();
		double q3_c=-1.0*_IMU.f_rotation.z();


		//////////////////////////////////////////////////////////////erase this try


		Eigen::Quaterniond Q(q0_c,q1_c,q2_c,q3_c);

		Q.normalize();                                                            //to represent rotation we need to normalize it

		Eigen::Vector3d acc(lin_acc.x,lin_acc.y,lin_acc.z);

		Eigen::Vector3d rttdAcc;                                                  //Rotated acceleration

		_IMU.rotateVector3dByQuat(Q,acc,rttdAcc);

		double aox=rttdAcc[0];
		double aoy=rttdAcc[1];
		double aoz;
		double gravity_opt1,gravity_opt2,gravity_opt3;
		if(option==1){
			aoz=rttdAcc[2]+9.80665/1.9; // Gravity acceleration compensation
		}
		if(option==2||option==3){
			np.getParam("gravity_opt2",gravity_opt2);
			aoz=rttdAcc[2]+gravity_opt2;
		}

		//This function integrate the acc to obtain the position
		_IMU.integratePosition(aox,aoy,aoz);

		if(option==3)
		{
			double mod = count % 8;                                               // To take 1 out of 8 frames in order to be updated

			//ROS_INFO("IMU px: [%lf], py [%lf], pz [%lf], dt: [%lf]",_IMU.px,_IMU.py,_IMU.pz,_IMU.dt);
			//ROS_INFO("IMU q0: [%lf], q1: [%lf], q2: [%lf],q3: [%lf]",q0,q1,q2,q3);

			if(mod==0.0){                                                         // Each 8 frames the positions is updated

				_IMU.px=nav_update.real_pos.translation.x;
				_IMU.py=nav_update.real_pos.translation.y;
				_IMU.pz=nav_update.real_pos.translation.z;
				double r_q0=nav_update.real_pos.rotation.w;
				double r_q1=nav_update.real_pos.rotation.x;
				double r_q2=nav_update.real_pos.rotation.y;
				double r_q3=nav_update.real_pos.rotation.z;
				//filter_.setOrientation(r_q0,r_q1,r_q2,r_q3);
				//ROS_INFO("Update px: [%lf], py [%lf], pz [%lf], dt: [%lf]",_IMU.px,_IMU.py,_IMU.pz,_IMU.dt);
				//ROS_INFO("Update q0: [%lf], q1: [%lf], q2: [%lf],q3: [%lf]",r_q0,r_q1,r_q2,r_q3);
			}
			//ROS_INFO("count: [%d]",count);
			//ROS_INFO("Mod: [%lf] ",mod);
			//ROS_INFO("..............................................");

		}
		filter_.getOrientation(q0,q1,q2,q3);

		// This is no needed
		_IMU.est_pose.pose.position.x=_IMU.px;
		_IMU.est_pose.pose.position.y=_IMU.py;
		_IMU.est_pose.pose.position.z=_IMU.pz;

		_IMU.est_pose.pose.orientation.x=q1;
		_IMU.est_pose.pose.orientation.y=q2;
		_IMU.est_pose.pose.orientation.z=q3;
		_IMU.est_pose.pose.orientation.w=q0;

		//ROS_INFO("IMU ax: [%lf], ay: [%lf], az: [%lf]",lin_acc.x,lin_acc.y,lin_acc.z);
		//ROS_INFO("IMU aox: [%lf], aoy: [%lf], aoz: [%lf]",aox,aoy,aoz);
		//ROS_INFO("IMU tl_x: [%lf], tl_y: [%lf], tl_z: [%lf]",_IMU.tl_x,_IMU.tl_y,_IMU.tl_z);
		//ROS_INFO("IMU px: [%lf], py [%lf], pz [%lf], dt: [%lf]",_IMU.px,_IMU.py,_IMU.pz,_IMU.dt);
		//ROS_INFO("IMU p0_x: [%lf], p0_y [%lf], p0_z [%lf]",_IMU.p0_x,_IMU.p0_y,_IMU.p0_z);
		//ROS_INFO("IMU q0: [%lf], q1: [%lf], q2: [%lf],q3: [%lf]",q0,q1,q2,q3);
		//ROS_INFO("-----------------------------------------------------");

		Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

		pose.translation().x()=_IMU.px;
		pose.translation().y()=_IMU.py;
		pose.translation().z()=_IMU.pz;

		//ROTATE THE ORIENTATION

		double yaw,pitch,roll;

		pose.rotate( Qviz.toRotationMatrix() );

		if(imu_norm){
			_poses_estimated.push_back(pose);
		}
	}
	if (option==4||option==5){
		if(count==0){
			ROS_INFO("Entra en count 0 option 4");
			//Define the offset input sensor in order to calibrate it
			_IMU.x_acc_offset=lin_acc.x;
			_IMU.y_acc_offset=lin_acc.y;
			_IMU.z_acc_offset=lin_acc.z;

			_IMU.x_rot_offset=ang_vel.x;
			_IMU.y_rot_offset=ang_vel.y;
			_IMU.z_rot_offset=ang_vel.z;

			if(option==5){
				_IMU.z_acc_offset=0;
			}

			//Initialize the first of the body reference frame
			//ZYX: 180º 0º 90º
			geometry_msgs::Quaternion init_q;
			init_q.w=0;
			init_q.x=0;
			init_q.y=0.7071068;
			init_q.z=0.7071068;
			filter_.setOrientation(init_q.w, init_q.x, init_q.y, init_q.z);

			_IMU.p0_x=0;
			_IMU.p0_y=0;
			_IMU.p0_z=0;

			_IMU.px=_IMU.p0_x;
			_IMU.py=_IMU.p0_y;
			_IMU.pz=_IMU.p0_z;

			_IMU.vx=0;
			_IMU.vy=0;
			_IMU.vz=0;

		}
		double x_rot=ang_vel.x-_IMU.x_rot_offset;
		double y_rot=ang_vel.y-_IMU.y_rot_offset;
		double z_rot=ang_vel.z-_IMU.z_rot_offset;

		double x_l_acc=lin_acc.x-_IMU.x_acc_offset;
		double y_l_acc=lin_acc.y-_IMU.y_acc_offset;
		double z_l_acc=lin_acc.z-_IMU.z_acc_offset;

		ROS_INFO("raw ax: [%lf], raw ay: [%lf], raw az: [%lf]",lin_acc.x,lin_acc.y,lin_acc.z);
		ROS_INFO("calibrated ax: [%lf], raw ay: [%lf], raw az: [%lf]",x_l_acc,y_l_acc,z_l_acc);

		float dt = _IMU.dt;
		filter_.madgwickAHRSupdateIMU(x_rot,y_rot,z_rot,x_l_acc,y_l_acc,z_l_acc,dt);

		double q0,q1,q2,q3;
		filter_.getOrientation(q0,q1,q2,q3);

		Eigen::Quaterniond EstimatedQ(q0,q1,q2,q3);

		EstimatedQ.normalize();

		Eigen::Quaterniond RotBack = EstimatedQ.conjugate();

		RotBack.normalize();

		Eigen::Vector3d acceleration(x_l_acc,y_l_acc,z_l_acc);

		acceleration.transpose();

		Eigen::Vector3d RotatedAcceleration;

		RotatedAcceleration = RotBack.toRotationMatrix() * acceleration;

		//Eigen::Vector3d RotatedAcc = RotatedAcceleration.vec();

		double aox = RotatedAcceleration[0];
		double aoy = RotatedAcceleration[1];
		double aoz = RotatedAcceleration[2];

		ROS_INFO("aox: [%lf], aoy: [%lf], aoz: [%lf]",aox,aoy,aoz);

		_IMU.integratePosition(aox,aoy,aoz);

		Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

		pose.translation().x()=_IMU.px;
		pose.translation().y()=_IMU.py;
		pose.translation().z()=_IMU.pz;

		ROS_INFO("px: [%lf], py: [%lf], pz: [%lf]",_IMU.px,_IMU.py,_IMU.pz);
		ROS_INFO("....................................");

		pose.rotate(EstimatedQ.toRotationMatrix());

		_poses_estimated.push_back(pose);

}

	count++;
	np.setParam("count",count);

}

//##########################################  MAIN  ##########################################################
//############################################################################################################

int32_t main(int32_t argc_, char** argv_) {

	//Load the parameters from the yaml file
	YAML::Node configuration = YAML::LoadFile("/home/carlos/catkin_ws/src/imu_estimator/src/parameter.yaml");

	// IMU_estimation VS Laser_Leica (option==1),IMU_estimation VS Vicon_Room (option==2)
	int option = configuration["main_parameters"]["option"].as<int>();
	double gravity_opt1 =  configuration["main_parameters"]["gravity_opt1"].as<double>();
	double gravity_opt2 =  configuration["main_parameters"]["gravity_opt2"].as<double>();

	ros::init(argc_, argv_, "imu_integrator_node");

	ros::NodeHandle node_laser;
	ros::NodeHandle node_imu;
	ros::NodeHandle node_V1;
	ros::NodeHandle np;

	int count=0;                                                              //Initialize the loop counter
	int countV1=0;

	//Initial parameters

	np.setParam("gravity_opt1",gravity_opt1);                                 // Set the initial parameters
	np.setParam("gravity_opt2",gravity_opt2);
	np.setParam("option",option);
	np.setParam("count",count);
	np.setParam("countV1",countV1);


	std::string topic_real_pos;
	std::string topic_estimated_pos;

	// Change the topics in function of the chose option
	if(option==1){                                                            // From DatasetA_0_100.bag
		topic_estimated_pos="/sensor/camera/vi_sensor/imu";
		topic_real_pos="/sensor/laser/leica/position";
	}else if (option==2||option==3) {                                         // From V1_01_easy.bag
		topic_estimated_pos="/imu0";
		//topic_estimated_pos="/fcu/imu";
		topic_real_pos="/vicon/firefly_sbx/firefly_sbx";
	}else if(option==4||option==5){
		topic_estimated_pos="/imu0";
	}

	std::cerr<<" Chose option: "<<option<<std::endl;

	//if(option==1){
	//	ros::Subscriber subs_laser = node_laser.subscribe(topic_real_pos,10,callbackPointStamped); //Dataset (laser Leica)
	//}else if(option==2){
	//ros::Subscriber subs_V1 = node_V1.subscribe(topic_real_pos,10,callbackTransformStamped);       //vicon
	//}
	ros::Subscriber subs_imu = node_imu.subscribe(topic_estimated_pos,10,filter_function);

	// VIEWER INITIALIZATION
	poses_ground_truth.clear();
	_poses_estimated.clear();

	//ds check for invalid call and display help
	if (argc_ > 2) {
		std::cerr << "trajectory_viewer\n"
				"shows a sequence of randomlyrated 3D (camera) poses \n"
				"usage: \n"
				" ./trajectory_viewer <number_of_generated_poses>" << std::endl;
		return 0;
	}

	//ds launch Qt
	QApplication app(argc_, argv_);

	//ds create our viewer
	std::cerr << "Qt initialized, launching viewer" << std::endl;
	TrajectoryViewer viewer;
	viewer.show();

	while (viewer.isVisible() && node_imu.ok()) {

		//ds trigger callbacks
		ros::spinOnce();

		//ds update viewer data
		viewer.updatePoses(poses_ground_truth);
		viewer.updatePoses_est(_poses_estimated);

		//ds update visualization
		viewer.updateGL();
		app.processEvents();
	}
	std::cerr << "terminated viewer" << std::endl;
	return 0;

}
