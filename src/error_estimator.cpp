#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include <Eigen/Geometry>
#include <time.h>
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Geometry>


#include "std_msgs/Float64.h"
#include "std_msgs/Header.h"
#include "geometry_msgs/Vector3.h"
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include "math.h"
#include <cmath>

double px_est,py_est,pz_est; // Estimated position
double px_real,py_real,pz_real; // Real position


void callbackPointStamped(const geometry_msgs::PointStampedConstPtr& position_){

    px_real=(position_->point.x)-(-5.21052811504); //Initial position of the laser
    py_real=(position_->point.y)-(3.31002023107);
    pz_real=(position_->point.z)-(1.68240086092);

}

void callbackStimatedPose(const geometry_msgs::PoseStampedConstPtr& msg){
    px_est=msg->pose.position.x;
    py_est=msg->pose.position.y;
    pz_est=msg->pose.position.z;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Statistic_class");
    ros::NodeHandle n;
    ros::NodeHandle np;
    ros::Subscriber sub_estimated = n.subscribe("/imu_estimated_pose", 1000,callbackStimatedPose);

    ros::Subscriber sub_real = np.subscribe("/sensor/laser/leica/position", 1000, callbackPointStamped);

    double err_abs_x,err_abs_y,err_abs_z;
    double err_rel_x,err_rel_y,err_rel_z;


    ros::Rate r(1);
    while(n.ok()){
        ros::spinOnce();

            err_abs_x=px_est-px_real;
            err_abs_y=py_est-py_real;
            err_abs_z=pz_est-pz_real;

            err_rel_x=fabs((err_abs_x)/px_real)*100;
            err_rel_y=fabs((err_abs_y)/py_real)*100;
            err_rel_z=fabs((err_abs_z)/pz_real)*100;



            //ROS_INFO("err_x: [%lf], err_y: [%lf], err_z:[%lf]",err_rel_x,err_rel_y,err_rel_z);
ROS_INFO("px_real: [%lf],py_real: [%lf],pz_real: [%lf]",px_real,py_real,pz_real);

        r.sleep();
    }
    return 0;
}
