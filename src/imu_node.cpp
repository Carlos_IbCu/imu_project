#include "imu_node.hpp"

imu_node::imu_node() :
    loop(0),count(0),vx(0.0),vy(0.0),vz(0.0),p0_x(0.0),p0_y(0.0),p0_z(0.0),tl_x(0.0),tl_y(0.0),
	tl_z(0.0),pp_x(0.0),pp_y(0.0),pp_z(0.0),cp_x(0.0),cp_y(0.0),cp_z(0.0),px(0.0),py(0.0),pz(0.0),
	x_acc_offset(0.0),y_acc_offset(0.0),z_acc_offset(0.0),x_rot_offset(0.0),y_rot_offset(0.0),
	z_rot_offset(0.0),p0x(0.0),p0y(0.0),p0z(0.0),pitch_norm(0.0),yaw_norm(0.0),roll_norm(0.0),
	dt(0.0)
{
}

imu_node::~imu_node()
{
}


