#pragma once


#include <iostream>
#include <cmath>
#include <Eigen/Geometry>
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/QuaternionStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Transform.h"
#include <yaml-cpp/yaml.h>


class imu_node
{
public:

	imu_node();                                  //Constructor
	virtual ~imu_node();
public:

	//Laser state variables
	int loop;                                    //Indicate in which loop we are
	double cp_x,cp_y,cp_z;                       //Current position
	double pp_x,pp_y,pp_z;                       //Previous position
	double p0_x,p0_y,p0_z;                       //Initial position of the laser

	//Imu state variables
	double count;
	double px,py,pz;                                     //Current pose
	geometry_msgs::PoseStamped est_pose;                 //Estimated pose
	float dt;                                            //Time variation between frames
	sensor_msgs::Imu imu_;
	Eigen::Quaterniond qNorm;
	double yaw_norm,pitch_norm,roll_norm;                //Euler angles
	double p0x,p0y,p0z;
	double tl_x,tl_y,tl_z;                               //Displacement between frames
	double x_acc_offset,y_acc_offset,z_acc_offset;       //Acceleration bias
	double x_rot_offset,y_rot_offset,z_rot_offset;       //Gyroscope bias
	double vx,vy,vz;                                     //Velocity
	Eigen::Quaterniond p_rotation,c_rotation,f_rotation;

	//Navigation update
	geometry_msgs::Transform real_pos;                   //Estimated position by the ProSlam

public:

	//Function integrate the oriented acceleration into the Inertial reference frame to get the displacement in each axis
	void integratePosition(double aox,double aoy, double aoz){

		this->vx+=(aox*this->dt);
		this->vy+=(aoy*this->dt);
		this->vz+=(aoz*this->dt);

		this->tl_x=((aox*this->dt)*this->dt)+this->vx*dt;
		this->tl_y=((aoy*this->dt)*this->dt)+this->vy*dt;
		this->tl_z=((aoz*this->dt)*this->dt)+this->vz*dt;

		this->px=(this->px+this->tl_x)-this->p0x;
		this->py=(this->py+this->tl_y)-this->p0y;
		this->pz=(this->pz+this->tl_z)-this->p0z;
	}

	//Fuction to obtain the Euler angles from a quaternion
	static void quat2euler(const Eigen::Quaterniond& q, double& roll, double& pitch, double& yaw)
	{
		// roll (x-axis rotation)
		double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
		double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
		roll = atan2(sinr_cosp, cosr_cosp);

		// pitch (y-axis rotation)
		double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
		if (fabs(sinp) >= 1)
			pitch = copysign(M_PI / 2, sinp);    // use 90 degrees if out of range
		else
			pitch = asin(sinp);

		// yaw (z-axis rotation)
		double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
		double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());
		yaw = atan2(siny_cosp, cosy_cosp);
	}

	//Function to rotate a vector3d by a Quaternion, to expres acceleration in the inertial frame
	static void rotateVector3dByQuat(const Eigen::Quaterniond& Q,const Eigen::Vector3d& acc,Eigen::Vector3d& result){
		// Q must be normalized!
		Eigen::Quaterniond P;
		P.w()=0;
		P.vec()=acc;
		Eigen::Quaterniond rotatecAcc = Q * P * Q.conjugate();
		Eigen::Vector3d result1 = rotatecAcc.vec();
		result=result1;
	}
};


