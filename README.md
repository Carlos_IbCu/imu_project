In order to launch the .bag file and the imu integrator:
"roslaunch imu_estimator imu_integrator_node"


If it doesn't work just change the path to the bag file in imu_integrator_node.launch

Option menu include in which option 1 is IMU_INTEGRATION VS LASER LEICA
Option menu include in which option 2 is IMU_INTEGRATION (without correction) VS VICON
Option menu include in which option 3 is IMU_INTEGRATION (with correction) VS VICON

To change the option you must change it in the parameters.yaml

For option 1 you must uncomment the subscriber in the line 520 of the imu_integration_node and comment the subscriber of the line 522.
The option 2 diverge from the real path at few seconds from the beginning due to the sudden turns that the drone does
The option 3 give some crazy points at a certain moment

option 4 = Integration of the VI sensor data acquired in the Uni
